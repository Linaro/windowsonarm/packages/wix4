#!/usr/bin/env bash

set -euo pipefail
set -x

script_dir=$(dirname $(readlink -f $0))

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 2 ] || die "usage: version out_dir"
version=$1; shift
out_dir=$1; shift

checkout()
{
    git clone https://github.com/wixtoolset/wix4
    cd wix4
    git checkout $version
    git log -n1
    # deactivate some hanging tests
    sed -i src/libs/libs.cmd -e '/DUtilUnitTest.dll/d'
    cd ..
}

build()
{
    cd wix4
    cat > build.bat << EOF
:: keep git in PATH
set __VSCMD_PREINIT_PATH=%PATH%
call vcvarsall.bat /clean_env || exit 1
call devbuild.cmd || exit 1
exit %ERRORLEVEL%
EOF
    cmd.exe /c build.bat

    file build/wix/Debug/publish/wix/wix
    build/wix/Debug/publish/wix/wix --version
}

get_git_for_windows()
{
    wget https://github.com/git-for-windows/git/releases/download/v2.37.3.windows.1/PortableGit-2.37.3-64-bit.7z.exe
    mkdir git
    cd git
    7z x ../Portable*
    export PATH=$(pwd)/bin:$PATH
    export TERM=msys # needed to avoid blocking warning
    which git
    cd ..
}

get_node()
{
    wget https://nodejs.org/dist/v19.8.1/node-v19.8.1-win-x64.zip
    mkdir node
    pushd node
    unzip ../node*zip
    mv node-*/* .
    export PATH=$(pwd):$PATH
    popd
    node --version
}

get_git_for_windows # needed, else error with some paths
get_node # needed for some unit tests
checkout
build
